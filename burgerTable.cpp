#include "burgerTable.h"
#include <cmath>
#include <fstream>
#include <string>
#include <iostream>
#include <algorithm>
#include <vector>


using namespace std;

HashTable::HashTable(int m){
	maxSize = m;
	size = 0;
	id = 0;
	data = new BurgerNode*[maxSize];
	
	for(int i =0;i< maxSize;i++){
		data[i] = NULL;
	}
}

//void HashTable::Insert(string pn,string m,string c,int ec,Owner o){
//	int pos = hash(pn,maxSize);
//	HashNode *temp = new HashNode(pn,m,c,ec,o);
//	if(data[pos] == NULL){
//		data[pos] = temp;
//	}else{
//		temp->next = data[pos];
//		data[pos] = temp;
//	}
//}


unsigned int HashTable::hash(string key,int m){
	unsigned int result = 0;
	unsigned int length = key.length();
	
	for(int i =0;i<length;i++){
		int c = (int)((char)key[i]);
		int temp = (int)(c*pow((float)128,(float)(length -i -1))) % m;
		result += temp;
	}
	result %= m;
	return result;
}

void HashTable::InitializeData(){
	string fileName = "Data.txt";
	ifstream fileIn(fileName.c_str());
	if(fileIn){
		while(!fileIn.eof()){
			string t,n,p;
			getline(fileIn,t,':');
			getline(fileIn,n,':');
			getline(fileIn,p);
			Insert(t,n,stof(p),t);
		}
	}
	
	Insert("feed","lettuce",search("ingredient","lettuce")->getPrice(),"add1");
	Insert("feed","cucumber",search("ingredient","cucumber")->getPrice(),"add2");
	Insert("feed","onion",search("ingredient","onion")->getPrice(),"add3");
}

type_code HashTable::convert_code(string code){
	if(code == "meal") return meal;
	if(code == "cook") return cook;
	if(code == "source") return source;
	if(code == "order") return order;
	if(code == "feed") return feed;	
	if(code == "add") return add;	
	if(code == "ingredient") return ingredient;
}



bool HashTable::addMeal(string key,int c){	
	
	string name;

	switch(c){
		case 1:
			name = "breef";
			break;
		case 2:
			name = "chicken";
			break;
		case 3:
			name = "pork";
			break;
	}
	
	return addOrder(key,name,order);
}



bool HashTable::contain(string key,string type,string name){
	
	int pos = hash(type,maxSize);
	
	if(convert_code(type) == order){
		for(auto val = data[pos];val != NULL;val = val->next){
			if(val->desc == key){
				return true;
			}
		}
	}else if(convert_code(type) == feed){
		//
		for(auto val = data[pos];val != NULL;val = val->next){
			if(val->name == name){
				return true;
			}
		}
	}
	
	return false;
}

bool HashTable::addOrder(string key,string name,type_code t){
	
	string type = (t == order) ? "order"
				: "feed";
	int pos = hash(type,maxSize);
	
	if(contain(key,type,name)){
		if(type == "order"){
			for(auto val = data[pos];val != NULL;val = val->next){
				if(val->desc == key){
					val->name = name;
					val->price = search(key,name)->getPrice();
					return true;
				}	
			}
		}else{
			for(auto val = data[pos];val != NULL;val = val->next){
				if(val->name == name){
					val->name = name;
					val->quantity += 1;
					size++;
					return true;
				}	
			}
		}
	}else{
			Insert(type,name,search(key,name)->getPrice(),key);
			return true;
		}
		
		return false;
}


BurgerNode* HashTable::search(string key,string name){
	
	int pos = hash(key,maxSize);
	
	for(auto val = data[pos];val != NULL;val = val->next){
		if(val->type == key && val->name == name){
			return val;
		}
	}
}

void HashTable::Insert(string t,string n,float p,string d){
	
	
	
	int pos = hash(t,maxSize);
	string desc;
	if(convert_code(t) == feed){
		size++;
		id++;
		desc =  d+to_string(id);
	}else{
		desc = d;
	}
		
	BurgerNode *temp = new BurgerNode(t,n,p,desc);
	
	if(data[pos] == NULL){
			data[pos] = temp;
	}else{
		temp->next = data[pos];
		data[pos] = temp;
	}
}


void HashTable::checkBurgerStatus(){
	int pos = hash("order",maxSize);
	int popo = hash("feed",maxSize);
	total = 0;
	
	for(auto val = data[pos];val != NULL;val = val->next){
//		transform(val->desc.begin(), val->desc.end(),val->desc.begin(), ::toupper);
		cout << "\n=======================\n";
		cout <<val->desc<<endl;
		cout << "=======================\n";
		cout << val->name<<endl;
		cout << "Price:"<<val->price*val->quantity<<endl<<endl;
		total += val->price;
	}
	
	for(BurgerNode* v2= data[popo];v2 != NULL;v2 = v2->next){
		cout <<"Other feed:";
		cout <<v2->desc<<endl;
		cout <<v2->name<<endl;
		cout <<v2->price<<endl;
		cout <<"Quantity:"<<v2->quantity<<endl;
		total += v2->price*v2->quantity;
	}
	
	
	cout <<"Total Cost:"<<total;
}

bool HashTable::addMaterial(string key,int c){
	
	switch(convert_code(key)){
		case meal:
			addMeal(key,c);
			break;
		case cook:
			addCook(key,c);
			break;
		case source:
			addSource(key,c);
			break;
		case add:
			addFeed(key,c);
			break;
	}
}

bool HashTable::addFeed(string key,int c){
	
	string name = otherAddList(c).at(0);
	
	return addOrder(key,name,feed);
}

bool HashTable::addCook(string key,int c){	
	
	string name;

	switch(c){
		case 1:
			name = "rare";
			break;
		case 2:
			name = "medium";
			break;
		case 3:
			name = "well";
			break;
	}
	
	return addOrder(key,name,order);
}

bool HashTable::addSource(string key,int c){	
	
	string name;

	switch(c){
		case 1:
			name = "chili";
			break;
		case 2:
			name = "mustard";
			break;
		case 3:
			name = "tomato";
			break;
	}
	
	return addOrder(key,name,order);
}

vector<string> HashTable::otherAddList(int c){
	
	vector<string> name;
	
		switch(c){
		case 1:
			name.push_back("drink");
			break;
		case 2:
			name.push_back("cookies");
			break;
		case 3:
			name.push_back("muffin");
			break;
	}
	
	return name;
}

vector<string> HashTable::ingredientList(int c){
	
	vector<string> name;
	
		switch(c){
		case 1:
			name.push_back("lettuce");
			break;
		case 2:
			name.push_back("cucumber");
			break;
		case 3:
			name.push_back("onion");
			break;
		case 4:
			name.push_back("lettuce");
			name.push_back("cucumber");
			name.push_back("onion");
		case 5:
			break;
	}
	
	return name;
}

bool HashTable::removeOther(string key,int c,string type){
	
	vector<string> name;
	
	switch(convert_code(type)){
		case add:
			name = otherAddList(c);
			break;
		case ingredient:
			name = ingredientList(c);
			break;
	}
	
	int pos = hash(key,maxSize);
	int count = 0;
	BurgerNode* node = data[pos];
	
	if(node != NULL){
		for(auto val= node;val != NULL;val = val->next){
			count++;
			for(int i =0;i < name.size();i++){
				if(val->name == name[i]){
					val->quantity -= 1;
					if(val->quantity < 1){
						removeNode(node,val,count);
					}
					size--;
				}
			}
		}
		return true;
	}
	
	return false;
}

void HashTable::removeNode(BurgerNode* r,BurgerNode* n,int count){

	BurgerNode* temp = r;
	BurgerNode* prev;
	
    while (temp != NULL && temp->name != n->name) {
        prev = temp;
        temp = temp->next;
    }
	
	if(temp == r){
		r = temp->next;

	}else if(size == count){
		prev->next = NULL;
	}else{
		prev->next = temp->next;
	}
	
	size--;
	
	cout << "name pro"<<size<<endl;
	
	delete temp;	
}


