#include <iostream>
#include <string>
#include "burgerTable.cpp"

using namespace std;


int main(){
	HashTable *hashTable = new HashTable(30);
	hashTable->InitializeData();
//	hashTable->Lookup("meal");
	
	hashTable->addMaterial("meal",2);
	hashTable->addMaterial("cook",1);
	hashTable->addMaterial("source",3);
	hashTable->checkBurgerStatus();
	cout <<"\nNext turn===========================\n\n";
	hashTable->addMaterial("meal",1);
	hashTable->addMaterial("add",1);
	hashTable->addMaterial("add",2);
	hashTable->addMaterial("add",2);
	hashTable->checkBurgerStatus();
	cout <<"\n\n=============================================\n\n";
	cout <<"\nLast result:\n";
	hashTable->removeOther("feed",2,"add");
	hashTable->removeOther("feed",1,"add");
	hashTable->removeOther("feed",2,"add");
	hashTable->checkBurgerStatus();
	hashTable->addMaterial("add",3);
	hashTable->removeOther("feed",4,"ingredient");
	
	cout <<"\n\n\n===========end===========\n\n";
	hashTable->checkBurgerStatus();
	return 0;
}
