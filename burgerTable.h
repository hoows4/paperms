#pragma once

#include <iostream>
#include <string>
#include <vector>

using namespace std;

enum type_code{
	meal,
	cook,
	source,
	ingredient,
	add,
	feed,
	order
};

class BurgerNode{
	private:
		string type;
		string name;
		float price;
		string desc;
		int quantity;
		BurgerNode* next;
	public:
		BurgerNode(){
			type = "";
			name = "";
			price = 0.0;
			desc = "";
			quantity = 1;
		}
		BurgerNode(string t,string n,float p,string d){
			type = t;
			name = n;
			price = p;
			desc = d;
			quantity = 1;
		}
		float getPrice(){
			return price;
		}
		
		friend class HashTable;
};

class HashTable{
	private:
		BurgerNode **data;
		float total;
		int id;
	protected:
		int maxSize;
		BurgerNode *arr = nullptr;
		virtual bool addOrder(string key,string name,type_code t);
	public:
		int size;
		HashTable(int m);
		HashTable():data(nullptr),maxSize(0),size(0),total(0.00){}
		//only represent natural numbers (positive numbers and zero)
		unsigned int hash(string key,int m);
		virtual void InitializeData();
		virtual type_code convert_code(string code);
		virtual void Insert(string t,string n,float p,string d);
		virtual bool addMeal(string key,int c);
		virtual bool addCook(string key,int c);
		virtual bool addSource(string key,int c);
		virtual bool addMaterial(string key,int c);
		virtual void checkBurgerStatus();
		virtual BurgerNode* search(string key,string name);
		virtual bool contain(string key,string type,string name);
		virtual bool addFeed(string key,int c);
		virtual vector<string> otherAddList(int c);
		virtual vector<string> ingredientList(int c);
		virtual bool removeOther(string key,int c,string type);
		virtual void removeNode(BurgerNode* r,BurgerNode* n,int count);
};



